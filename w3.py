'''16. Write a Python program to get the difference between a given number and 17, 
    if the number is greater than 17 return double the absolute difference.
'''
def ex16(n):
    if(n>17):
        return 2*(n-17)
    else:
        return 17-n

'''20. Write a Python program to get a string which is 
    n (non-negative integer) copies of a given string
'''
def ex20(myStr, n):
    return myStr*n

'''21. Write a Python program to find whether a given number 
    (accept from the user) is even or odd, print out an appropriate message to the user.
'''
def ex21(n):
    if((n%2)==0):
        print("The number is even")
    else:
        print("The number is odd")

'''27. Write a Python program to concatenate all elements in a list into a string and return it.
'''
def ex27(myList):
    return "".join(myList)

'''34. Write a Python program to sum of two given integers. 
    However, if the sum is between 15 to 20 it will return 20.
'''
def ex34(n,m):
    sum=m+n
    if(sum>14 and sum<21):
        return 20
    else:
        return sum