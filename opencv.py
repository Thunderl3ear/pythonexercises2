import cv2 as cv

cap = cv.VideoCapture(0)

while True:
    _, frame = cap.read()
    
    cv.imshow("camera", frame)
    if cv.waitKey(1) == 27:     # Escape button
        break
cap.release()
cv.destroyAllWindows()
