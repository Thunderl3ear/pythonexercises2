# Python scripts
Very small scripts illustrating Python programming

## Table of Contents

- [Requirements](#requirements)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
* Requires `Python 3.6` or higher

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))
